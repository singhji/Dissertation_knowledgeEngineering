# -*- coding: utf-8 -*-
"""
Created on Mon Jun 11 19:43:45 2018

@author: jites
"""

from rdflib import Graph
import pprint
g = Graph()
g1 = Graph()
g2 = Graph()
g.parse("PollutionData.ttl", format="turtle")
g1.parse("WeatherData.ttl", format="turtle")
g2.parse("PatientData.ttl", format="turtle")
print(len(g),len(g1),len(g2))
g_m=g+g1
g_Union=g_m+g2
print(len(g_Union))

Patient = g2.query(
    """SELECT DISTINCT ?b ?c 
       WHERE {
           <http://example.org/data/PatientData.csv#PatientID=1> ?b ?c .
       }""")
Weather = g1.query(
    """SELECT DISTINCT  ?f ?g
       WHERE {
       <http://example.org/data/WeatherData.csv#Zone=Z1> ?f ?g .
       }""")
Pollution = g.query(
    """SELECT DISTINCT  ?f ?g
       WHERE {
         <http://example.org/data/PollutionData.csv#Zone=P1> ?f ?g .
       }""")

#qres=qres1+qres2

for _ in Patient:
    pprint.pprint(_)
for _ in Weather:
    pprint.pprint(_)
for _ in Pollution:
    pprint.pprint(_)
    
#?craft foaf:name "Apollo 7" .
#<http://example.org/data/PatientData.csv#PatientID=1> :Name "jitesh".
#<http://example.org/data/PatientData.csv#Name> "jitesh".
#FILTER (?population > 15000000) .
#<http://example.org/data/PollutionData.csv#Zone=P1> ?d ?e.
#<http://example.org/data/WeatherData.csv#Zone=Z1> ?f ?g .
                                                      
                                                      